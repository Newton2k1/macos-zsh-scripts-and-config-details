#!/bin/zsh

# Speichere den Pfad zur ISO-Datei und die erwartete Checksumme in Variablen
iso_path="$1"
expected_checksum="$2"

# Prüfe, ob die Datei existiert
if [[ ! -f "$iso_path" ]]; then
    echo "Die Datei '$iso_path' existiert nicht."
    exit 1
fi

# Berechne die SHA256-Checksumme der ISO-Datei
calculated_checksum=$(shasum -a 256 "$iso_path" | awk '{ print $1 }')

# Vergleiche die berechnete Checksumme mit der erwarteten Checksumme
if [[ "$calculated_checksum" == "$expected_checksum" ]]; then
    echo "gleich"
else
    echo "ungleich"
fi

