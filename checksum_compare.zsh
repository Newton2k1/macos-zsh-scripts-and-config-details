#!/usr/bin/env zsh

# Get the filename and website checksum from command-line arguments
filename=$1
website_checksum=$2

# Get the algorithm from the optional third argument (default is sha256)
# assigning the value of the third command-line argument ($3) to the algorithm variable. 
# If the third argument is not provided or is empty, it will default to the value 1
algorithm=${3:-1}

# Calculate the checksum of the file using the specified algorithm
checksum=$(shasum -a "$algorithm" "$filename" | awk '{print $1}')

# Convert the calculated checksum and website checksum to uppercase
checksum=$(echo "$checksum" | tr '[:lower:]' '[:upper:]')
website_checksum=$(echo "$website_checksum" | tr '[:lower:]' '[:upper:]')

# Compare the checksums and display the result
if [[ "$checksum" == "$website_checksum" ]]; then
    echo "Checksums match"
else
    echo "Checksums do not match"
fi

