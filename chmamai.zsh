# You can put files here to add functionality separated per file, which
# will be ignored by git.
# Files on the custom/ directory will be automatically loaded by the init
# script, in alphabetical order.

if [ -f ~/.aliases ]; then
    . ~/.aliases
fi

# For example: add yourself some shortcuts to projects you often work on.
#
# brainstormr=~/Projects/development/planetargon/brainstormr
# cd $brainstormr
#
plugins=(git git-auto-fetch)

ZSH_THEME=agnoster
#robbyrussell

