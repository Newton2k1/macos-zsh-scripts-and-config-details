echo "This script installs aliases and chmamai.zsh in the right places."
cp .aliases ~
cp .bashrc ~
cp .zshrc ~
cp chmamai.zsh ~/.oh-my-zsh/custom
source ~/.zshrc

