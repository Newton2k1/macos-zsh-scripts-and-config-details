This project shall comprise scripts (zsh and bash) to pre-configure the LINUX/UNIX 
for optimal work, as e.g. pre-set command abreviations and the like. 

MacOS may lack .bashrc and .zshrc files. The two files given can be used, if 
you don't have your own versions. Or you may want to add their content to your versions.

